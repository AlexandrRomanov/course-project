package ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements;

import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.LinkedHashSet;
import java.util.Set;

public class BinOp extends Expr<BinOp> {
    public String getSymbol() {
        return symbol;
    }

    protected String symbol;

    public Expr getLhs() {
        return lhs;
    }

    public void setLhs(Expr lhs) {
        this.lhs = lhs;
    }

    public Expr getRhs() {
        return rhs;
    }

    public void setRhs(Expr rhs) {
        this.rhs = rhs;
    }

    protected Expr lhs;
    protected Expr rhs;

    public BinOp(){}
    public BinOp(Expr lexpr, Expr rexpr, String symb) {
        lhs = lexpr; rhs = rexpr; symbol = symb;
    }

    public BinOp(BinOp bo) {
        lhs = bo.lhs.clone(); rhs = bo.rhs.clone(); symbol = new String(bo.symbol);
    }

    @Override
    public String toString() {
        return lhs.toString()+symbol+rhs.toString();
    }

    @Override
    public Set<Var> vars() {
        Set<Var> res = new LinkedHashSet<Var>();
        res.addAll(lhs.vars());
        res.addAll(rhs.vars());
        return res;
    }

    @Override
    public BinOp renameVar(Var x, Var y) {
        if(lhs.getClass() == Var.class || lhs.getClass() == Const.class) {
            if (lhs.getClass() == Var.class && lhs.equals(x)) {
                lhs = y;
            }
        } else {
            lhs.renameVar(x, y);
        }
        if(rhs.getClass() == Var.class || rhs.getClass() == Const.class) {
            if (rhs.getClass() == Var.class && rhs.equals(x)) {
                rhs = y;
            }
        } else {
            rhs.renameVar(x, y);
        }
        return this;
    }
}
