package ru.CourseProject.SSABuilder.Vertex.VertexTypes;

import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Expr;

import java.util.LinkedHashSet;
import java.util.Set;

public class Statement {
    private Var lhs;
    private Expr rhs;

    public Statement(Var var, Expr expr) {
        lhs = var; rhs = expr;
    }
    @Override
    public String toString() {
        if (rhs.toString() == null) {
            return null;
        }
        if (lhs() == null) {
            return "IF (" + rhs + ")";
        }
        return lhs+" <- "+rhs;
    }

    public Statement copy() {
        return new Statement(lhs==null ? null : new Var(lhs), rhs.clone());
    }
    public Expr rhs() {
        return rhs;
    }
    public Var lhs() {
        return lhs;
    }
    public Set<Var> vars() {
        Set<Var> res = new LinkedHashSet<Var>();
        if (lhs!=null)
            res.add(lhs);
        Set<Var> rhsVars = rhs.vars();
        res.addAll(rhsVars);
        return res;
    }
    public boolean isPhi() {
        return rhs.isPhi();
    }

    public Statement setLhs(Var l) {
        lhs = l;
        return this;
    }
    public Statement setRhs(Expr expr) {
        rhs = expr;
        return this;
    }

    public String toProgram() {
        if (rhs.toString() == null) {
            return "";
        }
        if (lhs() == null) {
            return "IF (" + rhs + ")\n";
        }
        return lhs+" = "+rhs + "\n";
    }
}
