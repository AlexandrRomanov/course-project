package ru.CourseProject.SSABuilder.Vertex.VertexTypes;

import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Expr;

import java.util.LinkedHashSet;
import java.util.Set;

public class Var extends Expr<Var> implements Comparable<Var> {
    private String name = "";
    private int version = -1;

    public Var(String n) {
        name = n;
    }
    public Var(String name, int version) {
        this.name = name;
        this.version = version;
    }
    public Var(Var var) {
        name = var.name;
        version = var.version;
    }

    public String toString() {
        return name+(0>version ? "" : version);
    }

    public int compareTo(Var o) {
        return name.compareTo(o.name);
    }

    public boolean equals(Var var) {
        return name.equals(var.name);
    }

    public Set<Var> vars() {
        Set<Var> res = new LinkedHashSet<Var>();
        res.add(this);
        return res;
    }
    public String name() {
        return name;
    }
    public Var newVersion(int ver) {
        version = ver;
        return this;
    }

    public int getVersion() {
        return version;
    }
}
