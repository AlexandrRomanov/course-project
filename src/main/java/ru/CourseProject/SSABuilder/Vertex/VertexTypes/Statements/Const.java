package ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements;

import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Const extends Expr<Const> {
    private double value = 0;
    public Const(double val) {
        value = val;
    }
    public Const(Const c) {
        value = c.value;
    }
    @Override
    public String toString() {
        return ""+value;
    }
    @Override
    public Set<Var> vars() {
        return new LinkedHashSet<Var>();
    }

    public double getValue() {
        return value;
    }
}
