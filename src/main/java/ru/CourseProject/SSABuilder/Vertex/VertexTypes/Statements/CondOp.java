package ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements;

public class CondOp extends BinOp {
    private Boolean state = false;
    private Boolean haveState = false;

    public CondOp(Expr lexpr, Expr rexpr, String symb) {
        lhs = lexpr; rhs = rexpr; symbol = symb;
    }

    public CondOp setState(Boolean state) {
        this.state = state;
        haveState = true;
        return this;
    }

    @Override
    public String toString() {
        if (haveState) {
            return state.toString();
        }
        return lhs.toString()+symbol+rhs.toString();
    }
}
