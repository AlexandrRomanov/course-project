package ru.CourseProject.SSABuilder.Vertex.VertexTypes;

import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Const;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Expr;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Func;

import java.util.ArrayList;
import java.util.List;

public class PhiFunc extends Func{

    protected final String name = "phi";
    public Var var = null;
    private List<Var> params = new ArrayList<Var>();
    private Const value = null;
    private int valueNum = 0;
    private boolean isProp = true;

    public PhiFunc(Var p, int k) {
        var = p;
        for (int i = 0; i < k; i++)
            params.add(new Var(p));
    }
    public PhiFunc(PhiFunc pf) {
        pf.params.forEach(arg -> params.add(arg.clone()));
    }
    @Override
    public String toString() {
        if(params.get(1).getVersion() == -1 || params.get(0).getVersion() == -1) {
            return null;
        }
        return name + "_(" + params + ")";
    }
    //	@Override
    public List<Var> args() {
        return params;
    }
    public Var getPhiFuncVar() {
        return var;
    }

    public void addValue(Const num) {
        System.out.println("!!!!");
        valueNum++;
        if (value == null) {
            value = num;
            return;
        }
        isProp = (value.getValue() == num.getValue()) && isProp;
    }

    public Const getValue() {
        if (valueNum == params.size() && isProp) {
            return value;
        }
        return null;
    }
}
