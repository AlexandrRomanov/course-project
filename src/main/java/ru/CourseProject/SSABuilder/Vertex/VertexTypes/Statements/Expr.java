package ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements;

import ru.CourseProject.SSABuilder.Vertex.VertexTypes.PhiFunc;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.Set;

public abstract class Expr<T extends Expr> implements Cloneable {
    @Override
    public T clone() {
        try {
            return (T) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isPhi() {
        return this.getClass().equals(PhiFunc.class);
    }

    public Set<Var> vars() {
        return null;
    }

    public T renameVar(Var x, Var y) {
        return (T) this;
    }
}
