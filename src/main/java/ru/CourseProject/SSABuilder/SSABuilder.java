package ru.CourseProject.SSABuilder;

import ru.CourseProject.SSABuilder.Vertex.Vertex;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.PhiFunc;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statement;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.BinOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.CondOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Expr;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.*;

public class SSABuilder {
    public CFG buildSSAform(CFG cfg) {
        CFG ssa = cfg;
        cfg.initFull();
        System.out.println(ssa);
        phiFuncAllocation(ssa);
        //CFG ssa1 = ssa.addVars();
        ssa.vars().forEach(x -> renameVar(ssa, x));
        return ssa;
    }

    private void phiFuncAllocation(CFG ssa) {
        Map<Var, Set<Vertex>> S = initS(ssa);
        System.out.println("[S-map]:\t");
        S.forEach((p, s) -> {
            System.out.print("\t["+p+"]:\t");
            s.forEach(v -> System.out.print(v.getUid()+"  "));
            System.out.println();
        });
        System.out.println();

        Map<Var, Set<Vertex>> J = initJ(ssa, S);
        System.out.println("[J-map]:");
        J.forEach((k,v) -> {
            System.out.print("\t["+k+"]:\t");
            v.forEach(vert -> System.out.print(vert.getUid()+"  "));
            System.out.println();
        });
        Set<Var> vars = ssa.vars();
        vars.forEach(p -> {
            Set<Vertex> j = J.get(p);

            j.forEach(v -> {
                v.addPhiFunc(p);
            });
        });
    }

    private Map<Var, Set<Vertex>> initS(CFG ssa) {
        Set<Var> vars = ssa.vars();
        Map<Var, Set<Vertex>> S = new HashMap<Var, Set<Vertex>>();
        vars.forEach(p -> {
            S.put(p, ssa.definitionVertex(p));
        });
        return S;
    }

    private Map<Var, Set<Vertex>> initJ(CFG ssa, Map<Var, Set<Vertex>> S) {
        Set<Var> vars = ssa.vars();
        Map<Var, Set<Vertex>> J = new HashMap<Var, Set<Vertex>>();
        vars.forEach(p -> {
            J.put(p, ssa.getDFP_Set(S.get(p)));
        });
        return J;
    }

    public void renameVar(CFG ssa, Var p) {
        int counter = 0;
        Stack<Integer> stack = new Stack<Integer> ();
        stack.push(null);
        traverse(ssa.entry(), p, counter, stack);
    }

    public CFG renameAll(CFG ssa, Var p) {
        int counter = 0;
        System.out.println(p);
        Stack<Integer> stack = new Stack<Integer> ();
        stack.push(null);
        traverseX(ssa.entry(), p, counter, stack);
        return ssa;
    }

    private void traverseX(Vertex v, Var p, int counter, Stack<Integer> stack)	{
        for(Statement s : v.getStmts()) {
            Set<Var> p2 = s.rhs().vars();
            //s.rhs().clone();
            if (!s.isPhi()) {
                for (Var p1 : p2)
                    if (p1.equals(p)) {
                        s.setRhs(s.rhs().renameVar(p1, new Var(p1.name(), stack.peek())));
                        //p1.newVersion(stack.peek());
                        //p1 = new Var(p1.name(), stack.peek());
                    }
            }
            if (s.lhs()!=null && s.lhs().equals(p)) {
                s.setLhs(new Var(s.lhs().name(), counter));
                stack.push(counter);
                counter++;
            }
        }
        v.getSucc().forEach(w -> {
            int j = w.whichPred(v);
            w.phis().forEach(phiStmt -> {
                PhiFunc phi = (PhiFunc)phiStmt.rhs();
                if (phiStmt.lhs()==p) {
                    List<Var> args = phi.args();
                    Var[] params = args.toArray(new Var[args.size()]);

                    if (stack.empty() || stack.size()<2)
                        params[j].newVersion(-1);
                    else
                        params[j].newVersion(stack.peek());
                }
            });
        });
        for(Vertex w : v.getChildren())
            traverse(w, p, counter, stack);
        v.getStmts().forEach(stmt -> {
            if (stmt.lhs() == p)
                stack.pop();
        });
    }

    private void traverse(Vertex v, Var p, int counter, Stack<Integer> stack)	{
        for(Statement s : v.getStmts()) {
            Set<Var> p2 = s.rhs().vars();
            if (!s.isPhi()) {
                for (Var p1 : p2)
                    if (p1.equals(p)) {
                        p1.newVersion(stack.peek());
                        //p1 = new Var(p1.name(), stack.peek());
                    }
            }
            if (s.lhs()!=null && s.lhs().equals(p)) {
                p.newVersion(counter);
                stack.push(counter);
                counter++;
            }
        }
        v.getSucc().forEach(w -> {
            int j = w.whichPred(v);
            w.phis().forEach(phiStmt -> {
                PhiFunc phi = (PhiFunc)phiStmt.rhs();
                if (phiStmt.lhs()==p) {
                    List<Var> args = phi.args();
                    Var[] params = args.toArray(new Var[args.size()]);

                    if (stack.empty() || stack.size()<2)
                        params[j].newVersion(-1);
                    else
                        params[j].newVersion(stack.peek());
                }
            });
        });
        for(Vertex w : v.getChildren())
            traverse(w, p, counter, stack);
        v.getStmts().forEach(stmt -> {
            if (stmt.lhs() == p)
                stack.pop();
        });
    }
}
