package ru.CourseProject.SSABuilder;

import ru.CourseProject.Optimization.LIM;
import ru.CourseProject.Parser.Parser;
import ru.CourseProject.SSABuilder.Vertex.Vertex;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statement;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.BinOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.CondOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Expr;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.*;

public class CFG {

    public Set<Vertex> definitionVertex(Var p) {
        return varDef.get(p);
    }
    private Vertex entry;

    public Set<Vertex> getPreorder() {
        return preorder;
    }

    private Set<Vertex> preorder = new LinkedHashSet<Vertex>();
    private Set<Vertex> postorder = new LinkedHashSet<Vertex>();
    private Set<Var> vars;
    private Map<Var, Set<Vertex>> varDef = new HashMap<Var, Set<Vertex>>();

    public CFG() { Vertex.newGraph(); }
    @Override
    public String toString() {
        if (preorder.isEmpty() && entry != null)
            DFS(entry);
        String res = "=============CFG=============\n";
        for (Vertex v : preorder)
            res += ""+v;
        return res;
    }
    public CFG copy() {
        Vertex e = copyGraph(entry);
        CFG clone = new CFG();
        clone.entry = e;
        System.out.println(e.toString());
        //clone.initFull();
        return clone;
    }
    private Vertex copyGraph(Vertex v) {
        System.out.println("7");
        Vertex v1 = v.copy();
        v.getSucc().forEach(w -> {
            v1.getSucc().add(copyGraph(w));
        });
        return v1;
    }
    public Vertex entry() {
        return entry;
    }
    public void initFull() {
        DFS(entry);
        System.out.print("[preorder]:\t");
        preorder.forEach(v -> System.out.print(v.getUid()+"  "));
        System.out.println();
        System.out.print("[postorder]:\t");
        postorder.forEach(v -> System.out.print(v.getUid()+"  "));
        System.out.println();

        initVarDef();
        System.out.println("[varDef]:");
        varDef.forEach((k,v) -> {
            System.out.print("\t["+k+"]:\t");
            v.forEach(vert -> System.out.print(vert.getUid()+"  "));
            System.out.println();
        });

        System.out.println("[Succ]:");
        preorder.forEach(v -> {
            System.out.print("\t["+v.getUid()+"]:\t");
            v.getSucc().forEach(vert -> System.out.print(vert.getUid()+"  "));
            System.out.println();
        });
        System.out.println("[Pred]:");
        preorder.forEach(v -> {
            System.out.print("\t["+v.getUid()+"]:\t");
            v.getPred().forEach(vert -> System.out.print(vert.getUid()+"  "));
            System.out.println();
        });

        initDoms();
        System.out.println("[Doms]:\t(getIdom) (isemidom)");
        preorder.forEach(v -> {
            System.out.println("\t["+v.getUid()+"]:\t"+v.getIdom().getUid()+"\t"+v.isemidom().getUid());
        });

        initDF();
        System.out.println("[DF]:");
        preorder.forEach(v -> {
            System.out.print("\t["+v.getUid()+"]:\t");
            v.getDF().forEach(vert -> System.out.print(vert.getUid()+"  "));
            System.out.println();
        });

        initChildren();
        System.out.println("[Children]:");
        preorder.forEach(v -> {
            System.out.print("\t["+v.getUid()+"]:\t");
            v.getChildren().forEach(vert -> System.out.print(vert.getUid()+"  "));
            System.out.println();
        });

        System.out.println("CFG features initialization done");
    }

    private void initDF() {
        postorder.forEach(x -> {
            x.getSucc().forEach(y -> {
                if (!y.getIdom().equals(x))
                    x.getDF().add(y);
            });
            x.getChildren().forEach(z ->
                    z.getDF().forEach(y -> {
                        if (!y.getIdom().equals(x))
                            x.getDF().add(y);
                    })
            );
        });
    }
    private Set<Vertex> getDF_Set(Set<Vertex> S) {
        Set<Vertex> res = new HashSet<Vertex>();
        S.forEach(k -> k.getDF().forEach(res::add));
        return res;
    }
    public Set<Vertex> getDFP_Set(Set<Vertex> S) {
        Set<Vertex> res = new LinkedHashSet<Vertex>();
        boolean change = true;
        Set<Vertex> DFP = getDF_Set(S);
        do {
            Set<Vertex> tmpSet = new HashSet<Vertex>(S);
            tmpSet.addAll(DFP);
            DFP = getDF_Set(tmpSet);
            if (change = DFP.size()!=res.size())
                res = DFP;
        } while(change);
        return res;
    }
    private void initVarDef() {
        preorder.forEach(v -> {
            v.varDef().forEach(p -> {
                Set<Vertex> val = varDef.get(p);
                if (val == null)
                    val = new LinkedHashSet<Vertex>();
                val.add(v);
                varDef.put(p,  val);
            });
        });
    }
    private void DFS(Vertex v) {
        if (!v.isMarked()) {
            preorder.add(v);
            v.mark();
            v.getSucc().forEach(w -> {
                w.getPred();
                w.getPred().add(v);
                if (!w.isMarked())
                    DFS(w);
            });
            postorder.add(v);
        }
    }
    private void initDoms() {
        preorder.forEach(v -> {
            v.findIsemidom();
        });
        preorder.forEach(v -> {
            v.findIdom();
        });
    }
    public void initChildren() {
        preorder.forEach(v -> {
            v.getIdom().getChildren().add(v);
        });
    }

    public Set<Var> vars() {
        if (vars==null)
            vars = vars(entry);
        return vars;
    }
    private static Set<Var> vars (Vertex v) {
        Set<Var> res = new LinkedHashSet<Var>();
        res.addAll(v.getVars());
        v.getSucc().forEach(
                w -> {
                    if (!w.isLocker()) {
                        res.addAll(vars(w));
                    }
                });
        return res;
    }


    public CFG addVars() {
        postorder.forEach(v -> {
            this.getPreorder().remove(v);
            Vertex tmp = addVarsToVertex(v);
            tmp.getSucc().addAll(v.getSucc());
            this.getPreorder().add(tmp);
        });
        return this;
    }

    private Vertex addVarsToVertex(Vertex vertex) {
        Vertex ans = vertex.copy();
        for (int i = 0; i < vertex.getStmts().size(); i++) {
            ans.getStmts().set(i, addVarsToStatement(vertex.getStmts().get(i)));
        }
        return ans;
    }

    private Statement addVarsToStatement(Statement stmt) {
        Statement ans = stmt.copy();
        if(ans.lhs() != null) {
            ans.setLhs(new Var(ans.lhs().name()));
        }
        ans.setRhs(addVarsToStatement(ans.rhs()));
        return ans;
    }

    private Expr addVarsToStatement(Expr expr) {
        if (expr.getClass() == Var.class) {
            return new Var(((Var) expr).name());
        }
        if (expr.getClass() == BinOp.class || expr.getClass() == CondOp.class) {
            return new BinOp(addVarsToStatement(((BinOp) expr).getLhs()), addVarsToStatement(((BinOp) expr).getRhs()), ((BinOp) expr).getSymbol());
        }
        return expr;
    }

    public static CFG createTestCFG(String prog) {
        CFG tstCFG = new CFG();
        Vertex v[] = new Parser(prog).parse();
        tstCFG.entry = v[0];
        return tstCFG;
    }

    public String toProgram() {
        String res = "==========New Program==========\n";
        res += entry.toProgram();
        return res;
    }
}
