package ru.CourseProject.Optimization;

import ru.CourseProject.SSABuilder.CFG;
import ru.CourseProject.SSABuilder.Vertex.Vertex;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.*;

public class CPO {
    private Expr getConst(Expr expr) {
        if(expr.getClass() == Const.class) {
            return expr;
        }
        if (expr.getClass() == CondOp.class) {
            return ((CondOp) expr).setState(((Const) ((BinOp) expr).getLhs()).getValue() < ((Const) ((BinOp) expr).getRhs()).getValue());
        }
        if (((BinOp) expr).getSymbol().equals("+")) {
            return new Const (((Const) ((BinOp) expr).getRhs()).getValue() + ((Const) getConst(((BinOp) expr).getLhs())).getValue());
        }
        return new Const (((Const) getConst(((BinOp) expr).getLhs())).getValue() - ((Const) ((BinOp) expr).getRhs()).getValue());
    }



    public void doCPO(Vertex v) {
        v.getStmts().forEach(stmt -> {
            if (stmt.rhs().vars().isEmpty() && !stmt.isPhi()) {
                stmt.setRhs(getConst(stmt.rhs()));
            }
        });
    }
}
