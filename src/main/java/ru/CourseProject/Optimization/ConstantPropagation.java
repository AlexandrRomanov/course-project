package ru.CourseProject.Optimization;

import ru.CourseProject.SSABuilder.CFG;
import ru.CourseProject.SSABuilder.Vertex.Vertex;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.PhiFunc;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statement;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.BinOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.CondOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Const;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Expr;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.HashMap;
import java.util.Map;

public class ConstantPropagation {

    private Expr replaceVar(Var var, Expr expr, Const cnst) {
        if (expr.equals(var)) {
            return cnst;
        }
        if (expr.getClass() == BinOp.class || expr.getClass() == CondOp.class) {
            ((BinOp) expr).setRhs(replaceVar(var, ((BinOp) expr).getRhs(), cnst));
            ((BinOp) expr).setLhs(replaceVar(var, ((BinOp) expr).getLhs(), cnst));
            return expr;
        }
        return expr;
    }

    private boolean checkVertex(Vertex vertex, Var var, Const cnst) {
        final boolean[] ans = {false};
        boolean changed = true;
        CPO cpo = new CPO();
        while (changed) {
            changed = false;
            for (Statement stmt : vertex.getStmts()) {
                if (stmt.lhs() == null) {
                    if (stmt.rhs().vars().contains(var) && cnst != null) {
                        System.out.println(var.toString());
                        stmt.setRhs(replaceVar(var, stmt.rhs(), cnst));
                        //changed = true;
                        //ans[0] = true;
                        System.out.println("!!!");
                    }
                    continue;
                }
                if (stmt.rhs().vars().contains(var) && cnst != null) {
                    System.out.println(var.toString());
                    stmt.setRhs(replaceVar(var, stmt.rhs(), cnst));
                    changed = true;
                    ans[0] = true;
                }
                if (stmt.lhs().equals(var)) {
                    if (stmt.isPhi()) {
                        ((PhiFunc) stmt.rhs()).addValue(cnst);
                        cnst = ((PhiFunc) stmt.rhs()).getValue();
                        if (cnst == null){
                            return false;
                        }
                        changed = true;
                        ans[0] = true;
                        continue;
                    }
                    if (stmt.rhs().getClass() == Const.class) {
                        cnst = (Const) stmt.rhs();
                    } else {
                        break;
                    }
                }
            }
            cpo.doCPO(vertex);
        }
        Const finalCnst = cnst;
        vertex.getSucc().forEach(v -> {
            ans[0] = ans[0] || checkVertex(v, var, finalCnst);
        });
        return ans[0];
    }

    public ConstantPropagation(CFG cfg) {
        final boolean[] changed = {true};
        while (changed[0]) {
            changed[0] = false;
            cfg.vars().forEach(var -> {
                changed[0] = changed[0] || checkVertex(cfg.entry(), var, null);
            });
        }
    }
}
