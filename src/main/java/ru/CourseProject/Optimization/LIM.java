package ru.CourseProject.Optimization;

import ru.CourseProject.SSABuilder.CFG;
import ru.CourseProject.SSABuilder.Vertex.Vertex;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statement;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.BinOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Const;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LIM {

    private CFG cfg;

    public LIM(CFG cfg) {
        this.cfg = cfg;
    }

    private boolean isInstInvar(Statement stmt) {
        return true;
    }

    private boolean isInvar(Statement stmt) {
        if(stmt.rhs().getClass().equals(Const.class)) {
            return true;
        }
        Iterator<Var> iterator = stmt.rhs().vars().iterator();
        while (iterator.hasNext()) {
            Var var = iterator.next();

        }
        return true;
    }

    private Map<Statement, Boolean> invarOrder() {
        Map<Statement, Boolean> instInvar = new HashMap<>();
        boolean change = false;
        for (Vertex v :
                cfg.getPreorder()) {
            for (Statement stmt :
                    v.getStmts()) {
                instInvar.put(stmt, false);
            }
        }
        do {
            change = false;
            //Что за Breadth_order
        }
        while (change);
        return instInvar;
    }

    public boolean markBlock(Vertex v) {
        boolean change = false;
        for (Statement stmt :
                v.getStmts()) {
            if (isInstInvar(stmt)) {
                boolean isInv = isInvar(stmt);
            }
        }
        return change;
    }

    private boolean domUses(Statement s) {
        return false;
    }

    private boolean domExits(Statement s) {
        return true;
    }

    public void Liiim() {
        Map<Statement, Boolean> instInvar = invarOrder();
        instInvar.forEach((stmt, ch) -> {
            if (domUses(stmt) && domExits(stmt) && ch) {
                //goToPreheader
            }
        });
    }
}
