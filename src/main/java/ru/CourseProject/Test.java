package ru.CourseProject;

import ru.CourseProject.Optimization.CPO;
import ru.CourseProject.Optimization.ConstantPropagation;
import ru.CourseProject.SSABuilder.CFG;
import ru.CourseProject.SSABuilder.SSABuilder;
import ru.CourseProject.SSABuilder.Vertex.Vertex;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

class Test {
    public static void main(String []args) {
        Vertex.initNull();
        String prog = "";
        File file = new File("prog.txt");
        try {
            List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
            for (String line: lines) {
                prog += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        CFG tstCFG1 = CFG.createTestCFG(prog);
        SSABuilder ssaBuilder = new SSABuilder();
        CFG ssa = ssaBuilder.buildSSAform(tstCFG1);
        System.out.println("SSA FORM");
        //ssa = ssaBuilder
        //ssa = new CPO().doCPO(ssa);
        //new LIM(ssa);
        new ConstantPropagation(ssa);
        ssa.vars().forEach(x -> ssaBuilder.renameAll(ssa, x));
        System.out.println(ssa);
        System.out.println(ssa.toProgram());
        //qerifhoqehfuherfuohrouf
        //erfefeferf
        System.out.println("erferferf");
        //wedwed
    }
}
