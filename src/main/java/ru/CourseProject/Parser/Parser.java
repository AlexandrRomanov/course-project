package ru.CourseProject.Parser;

import ru.CourseProject.SSABuilder.Vertex.Vertex;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statement;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.BinOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.CondOp;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Const;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Statements.Expr;
import ru.CourseProject.SSABuilder.Vertex.VertexTypes.Var;

import java.util.*;

public class Parser {
    public enum Lexems {
        NUM,
        IF,
        VAR,
        ELSE,
        PLUS,
        MINUS,
        MUL,
        LESS,
        EQUAL,
        LBRA,
        RBRA,
        LPAR,
        RPAR,
        SEMICOLON,
        WHILE
    }

    private String program;

    private int counter = 0;

    public Parser(String program) {
        this.program = program + "\n";
        Lexem curLexem = nextLexem();
        while (curLexem != null)
        {
            lexemList.add(new Lexem(curLexem.getName(), curLexem.getValue()));
            if(curLexem.getName() == Lexems.VAR) {
                vars.put(curLexem.getValue(), new Var(curLexem.getValue()));
            }
            curLexem = nextLexem();
        }
    }

    private Lexem nextLexem() {
        while (counter != program.length() && (program.charAt(counter) == '\n' || program.charAt(counter) == ' ')) {
            counter++;
        }
        if (counter == program.length()) {
            return null;
        }
        if(program.charAt(counter) >= '0' && program.charAt(counter) <= '9') {
            int begin = counter;
            while (program.charAt(counter) >= '0' && program.charAt(counter) <= '9') {
                counter++;
            }
            return new Lexem(Lexems.NUM, program.substring(begin, counter));
        }
        if(program.charAt(counter) >= 'a' && program.charAt(counter) <= 'z') {
            int begin = counter;
            while (program.charAt(counter) >= 'a' && program.charAt(counter) <= 'z') {
                counter++;
            }
            return new Lexem(Lexems.VAR, program.substring(begin, counter));
        }
        switch (program.charAt(counter)) {
            case 'I':
                counter+=2;
                return new Lexem(Lexems.IF, "IF");
            case 'E':
                counter+=4;
                return new Lexem(Lexems.ELSE, "ELSE");
            case '+':
                counter++;
                return new Lexem(Lexems.PLUS, "+");
            case '-':
                counter++;
                return new Lexem(Lexems.MINUS, "-");
            case '*':
                counter++;
                return new Lexem(Lexems.MUL, "*");
            case '=':
                counter++;
                return new Lexem(Lexems.EQUAL, "=");
            case '{':
                counter++;
                return new Lexem(Lexems.LBRA, "{");
            case '(':
                counter++;
                return new Lexem(Lexems.LPAR, "(");
            case ')':
                counter++;
                return new Lexem(Lexems.RPAR, ")");
            case '}':
                counter++;
                return new Lexem(Lexems.RBRA, "}");
            case '<':
                counter++;
                return new Lexem(Lexems.LESS, "<");
            case 'W':
                counter+=5;
                return new Lexem(Lexems.WHILE, "WHILE");
        }
        return null;
    }

    private ArrayList<Lexem> lexemList = new ArrayList();
    private int pointer = 0;
    private Map<String, Var> vars = new HashMap<>();

    private Expr term() {
        if (lexemList.get(pointer).getName() == Lexems.VAR) {
            pointer++;
            //return new Var(lexemList.get(pointer - 1).getValue());
            return vars.get(lexemList.get(pointer - 1).getValue());
        }
        pointer++;
        return new Const(Integer.parseInt(lexemList.get(pointer - 1).getValue()));
    }

    private Expr summa() {
        Expr n = term();
        while (lexemList.size() > pointer && (lexemList.get(pointer).getName() == Lexems.PLUS || lexemList.get(pointer).getName() == Lexems.MINUS)) {
            String value = lexemList.get(pointer).getValue();
            pointer++;
            n = new BinOp(n, term(), value);
        }
        return n;
    }

    private Expr test() {
        Expr n = summa();
        if (lexemList.size() > pointer && lexemList.get(pointer).getName() == Lexems.LESS) {
            pointer++;
            n = new CondOp(n, summa(), "<");
        }
        return n;
    }

    private Statement expr() {
        Var n = (Var) test();
        if(lexemList.size() > pointer && lexemList.get(pointer).getName() == Lexems.EQUAL) {
            pointer++;
            return new Statement(n, test());
        }
        return null;
    }

    private Statement paren_expr() {
        if (lexemList.get(pointer).getName() != Lexems.LPAR) {
            System.err.print("( expected");
        }
        pointer++;
        Statement n = new Statement(null, test());
        if (lexemList.get(pointer).getName() != Lexems.RPAR) {
            System.err.print(") expected");
        }
        pointer++;
        return n;
    }

    public Vertex[] parse() {
        Vertex ans[] = new Vertex[50];
        int vertexCounter = 0;
        Vertex curVertex = new Vertex();
        for (; pointer < lexemList.size();) {
            switch (lexemList.get(pointer).getName()) {
                case IF:
                    pointer++;
                    curVertex.addStmt(paren_expr());
                    ans[vertexCounter] = curVertex;
                    curVertex = null;
                    vertexCounter++;
                    if (lexemList.get(pointer).getName() != Lexems.LBRA) {
                        System.err.print("{ expected");
                    }
                    pointer++;
                    Vertex subStmt[] = parse();
                    int i = 0;
                    for (; subStmt[i] != null; i++) {
                        ans[vertexCounter + i] = subStmt[i];
                    }
                    ans[vertexCounter - 1].join(ans[vertexCounter]);
                    if (lexemList.get(pointer).getName() != Lexems.RBRA) {
                        System.err.print("} expected");
                    }
                    pointer++;
                    if (lexemList.size() != pointer && lexemList.get(pointer).getName() == Lexems.ELSE) {
                        pointer++;
                        if (lexemList.get(pointer).getName() != Lexems.LBRA) {
                            System.err.print("{ expected");
                        }
                        pointer++;
                        subStmt = parse();
                        vertexCounter+=i;
                        int j = 0;
                        for (; subStmt[j] != null; j++) {
                            ans[vertexCounter + j] = subStmt[j];
                        }
                        ans[vertexCounter - 1 - i].join(ans[vertexCounter]);
                        i = j;
                        if (lexemList.get(pointer).getName() != Lexems.RBRA) {
                            System.err.print("} expected");
                        }
                        pointer++;
                    }
                    subStmt = parse();
                    vertexCounter+=i;
                    int j = 0;
                    for (; subStmt[j] != null; j++) {
                        ans[vertexCounter + j] = subStmt[j];
                    }
                    //System.out.println(vertexCounter);
                    ans[vertexCounter - 1 - i].join(ans[vertexCounter]);
                    ans[vertexCounter - 1].join(ans[vertexCounter]);
                    return ans;
                    //break;
                case VAR:
                    curVertex.addStmt(expr());
                    break;
                case WHILE:
                    pointer++;
                    ans[vertexCounter] = curVertex;
                    vertexCounter++;
                    ans[vertexCounter] = new Vertex().addStmt(paren_expr());
                    ans[vertexCounter - 1].join(ans[vertexCounter]);
                    curVertex = null;
                    vertexCounter++;
                    if (lexemList.get(pointer).getName() != Lexems.LBRA) {
                        System.err.print("{ expected");
                    }
                    pointer++;
                    Vertex whStmt[] = parse();
                    int k = 0;
                    for (; whStmt[k] != null; k++) {
                        ans[vertexCounter + k] = whStmt[k];
                    }
                    ans[vertexCounter - 1].join(ans[vertexCounter]);
                    ans[vertexCounter - 1 + k].join(ans[vertexCounter - 1]).setLocker(true);
                    if (lexemList.get(pointer).getName() != Lexems.RBRA) {
                        System.err.print("} expected");
                    }
                    pointer++;
                    whStmt = parse();
                    vertexCounter+=k;
                    int m = 0;
                    for (; whStmt[m] != null; m++) {
                        ans[vertexCounter + m] = whStmt[m];
                    }
                    ans[vertexCounter - 1 - k].join(ans[vertexCounter]);
                    //System.out.println(ans[0].toString() + "!!!");
                    break;
                case RBRA:
                    ans[vertexCounter] = curVertex;
                    return ans;
            }
        }
        if(curVertex != null) {
            ans[vertexCounter] = curVertex;
        }
        return ans;
    }
}
