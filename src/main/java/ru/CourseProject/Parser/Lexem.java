package ru.CourseProject.Parser;

public class Lexem {
    private Parser.Lexems name;
    private String value;

    public Lexem(Parser.Lexems name, String value) {
        this.name = name;
        this.value = value;
    }

    public Parser.Lexems getName() {
        return name;
    }

    public void setName(Parser.Lexems name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
